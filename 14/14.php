<?php
// Отримати дані товарів з файлу
$products = [];
$file = fopen('products.txt', 'r');
if ($file) {
    while (($line = fgets($file)) !== false) {
        $productData = explode(',', $line);
        $productName = $productData[0];
        $productPrice = $productData[1];
        $productQuantity = $productData[2];
        $products[] = [
            'name' => $productName,
            'price' => $productPrice,
            'quantity' => $productQuantity
        ];
    }
    fclose($file);
}

// Додати вибрані товари у сесію
session_start();
if (isset($_POST['submit'])) {
    $selectedProducts = $_POST['products'];
    $_SESSION['selectedProducts'] = $selectedProducts;
}

// Відобразити товари
$selectedProducts = isset($_SESSION['selectedProducts']) ? $_SESSION['selectedProducts'] : [];
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Список товарів</title>
</head>
<body>
<h1>Список товарів</h1>
<form method="POST" action="">
    <?php foreach ($products as $product): ?>
        <label>
            <input type="checkbox" name="products[]" value="<?php echo $product['name']; ?>" <?php if (in_array($product['name'], $selectedProducts)) echo 'checked'; ?>>
            <?php echo $product['name']; ?> - <?php echo $product['price']; ?> грн
        </label>
        <br>
    <?php endforeach; ?>
    <br>
    <input type="submit" name="submit" value="Замовити">
</form>

<h2>Кошик</h2>
<?php foreach ($selectedProducts as $selectedProduct): ?>
    <p><?php echo $selectedProduct; ?></p>
<?php endforeach; ?>
</body>
</html>

