<?php
function get_users($filename)
{
    $users = [];
    $file = fopen($filename, 'r');

    if ($file) {
        while (($line = fgets($file)) !== false) {
            $user_data = explode('|', $line);
            $user = [
                'name' => $user_data[0],
                'login' => $user_data[1],
                'password' => $user_data[2],
                'email' => $user_data[3],
                'language' => $user_data[4]
            ];

            $users[] = $user;
        }

        fclose($file);
    } else {
        echo 'Помилка при відкритті файлу з користувачами.';
    }

    return $users;
}

function get_user_by_login_password($login, $password, $users)
{
    foreach ($users as $user) {
        if ($user['login'] === $login && $user['password'] === $password) {
            return $user;
        }
    }

    return 'Користувача не знайдено.';
}

$users = get_users('users.txt');

$user = get_user_by_login_password('johndoe', 'password123', $users);
if (is_array($user)) {
    echo 'Ім\'я: ' . $user['name'] . '<br>';
    echo 'Логін: ' . $user['login'] . '<br>';
    echo 'Пароль: ' . $user['password'] . '<br>';
    echo 'Емейл: ' . $user['email'] . '<br>';
    echo 'Мова: ' . $user['language'] . '<br><br>';
} else {
    echo $user;
}
?>
