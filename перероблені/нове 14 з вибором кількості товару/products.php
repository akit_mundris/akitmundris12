<?php
// Отримати дані товарів з файлу
$products = [];
$file = fopen('products.txt', 'r');
if ($file) {
    while (($line = fgets($file)) !== false) {
        $productData = explode(',', $line);
        $productName = $productData[0];
        $productPrice = $productData[1];
        $productQuantity = $productData[2];
        $products[] = [
            'name' => $productName,
            'price' => $productPrice,
            'quantity' => $productQuantity
        ];
    }
    fclose($file);
}

// Додати вибрані товари у сесію
session_start();
if (isset($_POST['submit'])) {
    $selectedProducts = [];
    foreach ($_POST['products'] as $productName => $quantity) {
        if ($quantity > 0) {
            $selectedProducts[$productName] = $quantity;
        }
    }
    $_SESSION['selectedProducts'] = $selectedProducts;
    header('Location: cart.php');
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Список товарів</title>
    <style>
        input[type="number"] {
            width: 40px;
        }
    </style>
</head>
<body>
<h1>Список товарів</h1>
<form method="POST" action="">
    <?php foreach ($products as $product): ?>
        <label>
            <?php echo $product['name']; ?> - <?php echo $product['price']; ?> грн
            <input type="number" name="products[<?php echo $product['name']; ?>]" min="0" value="<?php echo isset($_POST['products'][$product['name']]) ? $_POST['products'][$product['name']] : 0; ?>">
            уп.
        </label>
        <br>
    <?php endforeach; ?>
    <br>
    <input type="submit" name="submit" value="Замовити">
</form>
</body>
</html>
