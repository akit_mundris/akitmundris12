<?php
session_start();
$selectedProducts = isset($_SESSION['selectedProducts']) ? $_SESSION['selectedProducts'] : [];

// Отримати дані товарів з файлу
$products = [];
$file = fopen('products.txt', 'r');
if ($file) {
    while (($line = fgets($file)) !== false) {
        $productData = explode(',', $line);
        $productName = $productData[0];
        $productPrice = $productData[1];
        $productQuantity = $productData[2];
        $products[$productName] = [
            'price' => $productPrice,
            'quantity' => $productQuantity
        ];
    }
    fclose($file);
}

$totalPrice = 0;
foreach ($selectedProducts as $selectedProduct) {
    if (isset($products[$selectedProduct])) {
        $price = $products[$selectedProduct]['price'];
        $subtotal = $price * $products[$selectedProduct]['quantity'];
        $totalPrice += $subtotal;
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Кошик</title>
</head>
<body>
<h1>Кошик</h1>
<?php foreach ($selectedProducts as $selectedProduct): ?>
    <p><?php echo $selectedProduct; ?></p>
<?php endforeach; ?>
<p>Загальна сума: <?php echo $totalPrice; ?> грн</p>
</body>
</html>
