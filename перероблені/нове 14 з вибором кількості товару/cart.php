<?php
$selectedProducts = isset($_COOKIE['selectedProducts']) ? unserialize($_COOKIE['selectedProducts']) : [];

// Отримати дані товарів з файлу
$products = [];
$file = fopen('products.txt', 'r');
if ($file) {
    while (($line = fgets($file)) !== false) {
        $productData = explode(',', $line);
        $productName = $productData[0];
        $productPrice = $productData[1];
        $productQuantity = $productData[2];
        $products[$productName] = [
            'price' => $productPrice,
            'quantity' => $productQuantity
        ];
    }
    fclose($file);
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Кошик</title>
</head>
<body>
<h1>Кошик</h1>
<?php
$totalPrice = 0;
foreach ($selectedProducts as $productName => $quantity):
    if (isset($products[$productName])):
        $price = $products[$productName]['price'];
        $subtotal = $price * $quantity;
        $totalPrice += $subtotal;
        ?>
        <p><?php echo $productName; ?> - <?php echo $price; ?> грн x <?php echo $quantity; ?> - <?php echo $subtotal; ?> грн</p>
    <?php endif;
endforeach;
?>
<h2>Загальна сума: <?php echo $totalPrice; ?> грн</h2>
</body>
</html>

