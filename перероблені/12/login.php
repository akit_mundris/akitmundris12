<!DOCTYPE html>
<html>
<head>
    <title>Вхід</title>
</head>
<body>
<h2>Форма входу</h2>

<?php
// Отримати дані з форми
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $login = $_POST['login'];
    $password = $_POST['password'];

    // Функція для перевірки логіна та пароля
    function check_credentials($login, $password) {
        $file = file('users.txt', FILE_IGNORE_NEW_LINES);

        foreach ($file as $line) {
            list($storedLogin, $storedPassword) = explode(' ', trim($line));
            if ($login == $storedLogin && $password == $storedPassword) {
                return true;
            }
        }

        return false;
    }

    // Перевірити логін та пароль
    if (check_credentials($login, $password)) {
        // Вивести логін на екран
        echo "<p>Логін: $login</p>";

        // Зберегти кількість спроб входу у файл
        $attemptsFile = "$login.txt";
        if (file_exists($attemptsFile)) {
            $attempts = (int)file_get_contents($attemptsFile);
        } else {
            $attempts = 0;
        }

        $attempts++;

        file_put_contents($attemptsFile, $attempts);

        // Занести кількість спроб входу у JSON файл
        $jsonFile = 'attempts.json';
        $attemptsData = [];

        if (file_exists($jsonFile)) {
            $jsonContent = file_get_contents($jsonFile);
            $attemptsData = json_decode($jsonContent, true);
        }

        $attemptsData[$login] = $attempts;

        $jsonData = json_encode($attemptsData, JSON_PRETTY_PRINT);
        file_put_contents($jsonFile, $jsonData);
    } else {
        // Невірний логін або пароль
        echo "<p>Невірний логін або пароль</p>";
    }
}
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="login">Логін:</label>
    <input type="text" id="login" name="login" required><br><br>
    <label for="password">Пароль:</label>
    <input type="password" id="password" name="password" required><br><br>
    <input type="submit" value="Увійти">
</form>
</body>
</html>

