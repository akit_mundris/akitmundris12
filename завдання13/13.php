<?php

$users = [];

$filename = 'user.txt';
$file = fopen($filename, 'r');

if ($file) {
    while (($line = fgets($file)) !== false) {
        $user_data = explode('|', $line);
        $user = [
            'name' => $user_data[0],
            'login' => $user_data[1],
            'password' => $user_data[2],
            'email' => $user_data[3],
            'language' => $user_data[4]
        ];

        $users[] = $user;
    }

    fclose($file);

    $filtered_users = array_filter($users, function ($user) {
        return strlen($user['password']) >= 8;
    });

    foreach ($filtered_users as $user) {
        echo 'Ім\'я: ' . $user['name'] . '<br>';
        echo 'Логін: ' . $user['login'] . '<br>';
        echo 'Пароль: ' . $user['password'] . '<br>';
        echo 'Емейл: ' . $user['email'] . '<br>';
        echo 'Мова: ' . $user['language'] . '<br><br>';
    }
} else {
    echo 'Помилка при відкритті файлу з користувачами.';
}

